package com.kouchan.bookaride.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kouchan.bookaride.model.Driver;

public interface DriverRepository extends JpaRepository<Driver, Long> {

}
