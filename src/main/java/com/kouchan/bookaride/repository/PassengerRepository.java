package com.kouchan.bookaride.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kouchan.bookaride.model.Passenger;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

}
