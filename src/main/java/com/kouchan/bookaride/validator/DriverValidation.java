package com.kouchan.bookaride.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.kouchan.bookaride.model.Driver;

@Component
public class DriverValidation implements Validator {
	public boolean supports(Class clazz) {
		return Driver.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.firstName", "name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "error.lastName", "mobile number is required.");
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "error.email",
		// "Email is required.");
	}
}
