package com.kouchan.bookaride;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BookARideApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookARideApplication.class, args);
	}
}
