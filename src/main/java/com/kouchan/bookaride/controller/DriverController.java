package com.kouchan.bookaride.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.bookaride.exception.ResourceNotFoundException;
import com.kouchan.bookaride.model.Driver;
import com.kouchan.bookaride.repository.DriverRepository;
import com.kouchan.bookaride.validator.DriverValidation;

@RestController
@RequestMapping("/api/driver_api/")
public class DriverController {

	@Autowired
	DriverRepository driverRepository;

	DriverValidation driverValidation;

	// Get All Drivers
	@GetMapping("/drivers")
	public List<Driver> getAllDrivers() {
		return driverRepository.findAll();
	}

	// Create a new driver
	@PostMapping("/driver")
	public Driver createDriverModel(@Valid @RequestBody Driver driver) {
		return driverRepository.save(driver);
	}

	// get by id
	@GetMapping("/driver/{id}")
	public Driver getDriverById(@PathVariable(value = "id") Long id) {
		return driverRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Driver", "id", id));

	}

	// update driver details
	@PutMapping("/driver/{id}")
	public Driver updateDriver(@PathVariable(value = "id") Long id, @Valid @RequestBody Driver driverDetail) {

		Driver driver = driverRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Driver", "id", id));
		driver.setMobile(driverDetail.getMobile());
		driver.setName(driverDetail.getName());

		Driver updateDriver = driverRepository.save(driver);
		return updateDriver;
	}

	// delete driver
	@DeleteMapping("/driver/{id}")
	public ResponseEntity<?> deleteDriver(@PathVariable(value = "id") Long id) {

		Driver driver = driverRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Driver", "id", id));
		driverRepository.delete(driver);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/read")
	public String process(@RequestBody Map<String, Object> data, BindingResult result) {

		driverValidation.validate(data, result);
		return data.toString();
	}

	@PostMapping("/process") // spring 5 have brought this annotation
	public void processNew(@RequestBody JSONObject payload) throws Exception {
		String mobile = payload.getString("mobile");
		String name = payload.getString("name");
		System.out.println("mobile :" + mobile + " name :" + name);
	}

}
