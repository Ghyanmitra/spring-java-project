package com.kouchan.bookaride.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.bookaride.model.Passenger;
import com.kouchan.bookaride.repository.PassengerRepository;

@RestController
@RequestMapping("/api/passenger_api/")
public class PassengerController {

	@Autowired
	PassengerRepository passengerRepository;

	// Create a new driver
	@PostMapping("/register")
	public Passenger passengerRegistration(@Valid @RequestBody Passenger passenger) {
		return passengerRepository.save(passenger);
	}

}
