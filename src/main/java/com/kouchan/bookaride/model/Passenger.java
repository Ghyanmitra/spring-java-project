package com.kouchan.bookaride.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "passenger_details")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class Passenger {

	private static final String DEFAULT = "0";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String unique_id;

	@NotNull
	private String name;

	@NotNull
	private String mobile;

	@NotNull
	private String email;

	private String encrypted_password;

	private String salt;

	private String bdate;

	private String gender;

	private String messagingid;

	private String address;

	private String state;

	private String pin;

	@ColumnDefault("0.00")
	private String rating;

	@ColumnDefault("0")
	private Integer ride_cancel_count;

	@ColumnDefault("0")
	private String ride_status;

	@ColumnDefault("0")
	private String m3otp_status;

	@ColumnDefault("0")
	private String m3adhar_otp_status;

	@ColumnDefault("0")
	private String bar_otp_status;

	@ColumnDefault("0")
	private String block_status;

	private String login_time;

	private String device_id;

	@ColumnDefault("0")
	private String del_status;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;

	@Column(nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEncrypted_password() {
		return encrypted_password;
	}

	public void setEncrypted_password(String encrypted_password) {
		this.encrypted_password = encrypted_password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getBdate() {
		return bdate;
	}

	public void setBdate(String bdate) {
		this.bdate = bdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMessagingid() {
		return messagingid;
	}

	public void setMessagingid(String messagingid) {
		this.messagingid = messagingid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		if (rating == null) {
			rating = DEFAULT;
		}
		this.rating = rating;
	}

	public Integer getRide_cancel_count() {
		return ride_cancel_count;
	}

	public void setRide_cancel_count(Integer ride_cancel_count) {
		this.ride_cancel_count = ride_cancel_count;
	}

	public String getRide_status() {
		return ride_status;
	}

	public void setRide_status(String ride_status) {
		this.ride_status = ride_status;
	}

	public String getM3otp_status() {
		return m3otp_status;
	}

	public void setM3otp_status(String m3otp_status) {
		this.m3otp_status = m3otp_status;
	}

	public String getM3adhar_otp_status() {
		return m3adhar_otp_status;
	}

	public void setM3adhar_otp_status(String m3adhar_otp_status) {
		this.m3adhar_otp_status = m3adhar_otp_status;
	}

	public String getBar_otp_status() {
		return bar_otp_status;
	}

	public void setBar_otp_status(String bar_otp_status) {
		this.bar_otp_status = bar_otp_status;
	}

	public String getBlock_status() {
		return block_status;
	}

	public void setBlock_status(String block_status) {
		this.block_status = block_status;
	}

	public String getLogin_time() {
		return login_time;
	}

	public void setLogin_time(String login_time) {
		this.login_time = login_time;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getDel_status() {
		return del_status;
	}

	public void setDel_status(String del_status) {
		this.del_status = del_status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", unique_id=" + unique_id + ", name=" + name + ", mobile=" + mobile + ", email="
				+ email + ", encrypted_password=" + encrypted_password + ", salt=" + salt + ", bdate=" + bdate
				+ ", gender=" + gender + ", messagingid=" + messagingid + ", address=" + address + ", state=" + state
				+ ", pin=" + pin + ", rating=" + rating + ", ride_cancel_count=" + ride_cancel_count + ", ride_status="
				+ ride_status + ", m3otp_status=" + m3otp_status + ", m3adhar_otp_status=" + m3adhar_otp_status
				+ ", bar_otp_status=" + bar_otp_status + ", block_status=" + block_status + ", login_time=" + login_time
				+ ", device_id=" + device_id + ", del_status=" + del_status + ", createdAt=" + createdAt
				+ ", updatedAt=" + updatedAt + "]";
	}

}
